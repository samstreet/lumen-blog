<?php

declare(strict_types=1);

namespace App;

use App\Core\Exceptions\Handler as ExceptionHandler;
use Illuminate\Contracts\Console\Kernel as ConsoleKernelContract;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerContract;
use Jenssegers\Mongodb\MongodbServiceProvider;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Tymon\JWTAuth\Providers\JWTAuthServiceProvider;

/**
 * Class Application
 * @package App
 */
final class Application extends \Laravel\Lumen\Application
{
    /** @var array */
    private $localServiceProviders = [
    ];
    
    /** @var array */
    private $serviceProviders = [
        MongodbServiceProvider::class,
        Core\Providers\CoreServiceProvider::class,
        Authentication\Providers\AuthenticationServiceProvider::class,
        Authentication\Providers\AuthenticationRouteServiceProvider::class,
        User\Providers\UserServiceProvider::class,
        User\Providers\UserRouteServiceProvider::class,
        JWTAuthServiceProvider::class
    ];
    
    /**
     * @return void
     */
    public function registerLocalServiceProviders(): void
    {
        if ( ! $this->environment('production')) {
            foreach ($this->serviceProviders as $serviceProvider) {
                $this->register($serviceProvider);
            }
        }
    }
    
    /**
     * @return void
     */
    public function registerServiceProviders(): void
    {
        $this->singleton(ExceptionHandlerContract::class, ExceptionHandler::class);
        $this->singleton(ConsoleKernelContract::class, ConsoleKernel::class);
        foreach ($this->serviceProviders as $serviceProvider) {
            $this->register($serviceProvider);
        }
    }
    
}