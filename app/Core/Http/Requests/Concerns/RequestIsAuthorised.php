<?php

declare(strict_types=1);

namespace App\Core\Http\Requests\Concerns;

/**
 * Trait IsAuthorised
 * @package App\Core\Http\Concerns
 */
trait RequestIsAuthorised
{
    
    public function authorize(): bool
    {
        return true;
    }
    
}
