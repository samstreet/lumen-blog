<?php

declare(strict_types=1);

namespace App\Core\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
}
