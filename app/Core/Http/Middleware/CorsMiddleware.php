<?php

declare(strict_types=1);

namespace App\Core\Http\Middleware;

use Illuminate\Http\Request;

/**
 * Class CorsMiddleware
 */
class CorsMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        $response = $next($request);
        if ($request->isMethod('OPTIONS')) {
            $response = response('', 200);
        }
        
        $response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
        $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Expose-Headers', 'Location');
        
        return $response;
    }
    
}