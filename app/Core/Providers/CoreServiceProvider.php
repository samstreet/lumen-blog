<?php

declare(strict_types=1);

namespace App\Core\Providers;

use App\Application;
use App\Core\Http\Middleware\CorsMiddleware;
use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Cache\Factory as CacheFactory;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    
    use Concerns\HasAliases,
        Concerns\HasMiddleware;
    
    /**
     * {@inheritdoc}
     */
    protected $defer = true;
    
    /**
     * {@inheritdoc}
     */
    protected $aliases = [
        'cache' => [CacheFactory::class, CacheManager::class]
    ];
    
    /**
     * @var array
     */
    protected $middleware = [
        CorsMiddleware::class
    ];
    
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->bootConfiguration();
    }
    
    /**
     * @return void
     */
    protected function bootConfiguration(): void
    {
        $this->app->configure('api');
        $this->app->configure('app');
        $this->app->configure('filesystems');
        $this->app->configure('logging');
        $this->app->configure('schema');
    }
    
    /**
     * @return void
     */
    public function register(): void
    {
        $this->registerAliases();
        $this->registerCacheManager();
        $this->registerMiddleware();
    }
    
    /**
     * @return void
     */
    protected function registerCacheManager(): void
    {
        $this->app->singleton('cache', function (Application $app) {
            return new CacheManager($app);
        });
    }
    
}
