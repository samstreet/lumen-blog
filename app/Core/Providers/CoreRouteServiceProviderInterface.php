<?php

declare(strict_types = 1);

namespace App\Core\Providers;

/**
 * Interface RouteServiceProviderInterface
 * @package App\Package\Core\ServiceProvider
 */
interface CoreRouteServiceProviderInterface
{
    
    /**
     * Register routes during service bootstrapping.
     */
    public function boot(): void;
    
    /**
     * Get the router callable.
     *
     * @return \Closure
     */
    public function getRouteCallback(): \Closure;

}