<?php

declare(strict_types=1);

namespace App\Core\Providers\Concerns;

/**
 * Trait HasMiddleware
 * @package App\Core\Providers\Concerns
 */
trait HasMiddleware
{
    
    /**
     * @return void
     */
    public function registerMiddleware(): void
    {
        foreach($this->middleware as $middleware){
            $this->app->middleware($middleware);
        }
    }
    
}
