<?php
declare(strict_types=1);

namespace App\Core\Providers\Concerns;

/**
 * Trait HasAliases
 * @package App\Core\Providers\Concerns
 */
trait HasAliases
{
    
    /**
     * Register all aliases for this service provider.
     */
    public function registerAliases(): void
    {
        foreach ($this->aliases as $key => $aliases) {
            foreach ((array)$aliases as $alias) {
                $this->app->alias($key, $alias);
            }
        }
    }
    
    /**
     * Get the services provided.
     *
     * @return array
     */
    public function provides(): array
    {
        $interfaces = collect($this->aliases)->values()->flatten();
        
        return collect($this->aliases)->keys()->merge($interfaces)->toArray();
    }
    
}
