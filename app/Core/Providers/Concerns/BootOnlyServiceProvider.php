<?php

declare(strict_types = 1);

namespace App\Core\Providers\Concerns;

/**
 * Trait BootOnlyServiceProvider
 * @package App\Core\ServiceProviders\Concerns
 */
trait BootOnlyServiceProvider
{
    /**
     * Register the services.
     */
    public function register(): void
    {
    }

}
