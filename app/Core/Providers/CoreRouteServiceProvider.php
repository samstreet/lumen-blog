<?php

declare(strict_types=1);

namespace App\Core\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * @package App\Core\Providers\CoreRouteServiceProvider
 */
abstract class CoreRouteServiceProvider extends ServiceProvider implements CoreRouteServiceProviderInterface
{
    use Concerns\BootOnlyServiceProvider,
        Concerns\HasRouter;

    /**
     * The router attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * {@inheritdoc}
     */
    public function boot(): void
    {
        $this->getRouter()->group($this->attributes, $this->getRouteCallback());
    }

}
