<?php

declare(strict_types=1);

namespace App\Core\Exceptions;

use Exception;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Handler
 * @package App\Core\Exceptions
 */
class Handler extends ExceptionHandler
{
    
    public function render($request, Exception $e)
    {
        if($e instanceof NotFoundHttpException){
            return response()->json(
                [
                    "message" => "Not Found"
                ],
                $e->getStatusCode()
            );
        }
        
        return parent::render($request, $e);
    }
    
}
