<?php

declare(strict_types=1);

namespace App\Authentication\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller;

class AuthenticationController extends Controller
{
    
    /**
     * @return JsonResponse
     */
    public function issue(): JsonResponse
    {
        return response()->json(["test" => "test"]);
    }
    
}