<?php

declare(strict_types=1);

namespace App\Authentication\Service;

use League\OAuth2\Server\AuthorizationServer AS OAuth2AuthorizationServer;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

/**
 * Class AuthorizationService
 * @package App\Authentication\Service
 */
class AuthorizationService
{

    private $clientRepository;
    private $scopeRepository;
    private $accessTokenRepository;
    private $userRepository;
    private $refreshTokenRepository;
    
    /**
     * AuthorizationService constructor.
     *
     * @param ClientRepositoryInterface $clientRepository
     * @param ScopeRepositoryInterface $scopeRepository
     * @param AccessTokenRepositoryInterface $accessTokenRepository
     * @param UserRepositoryInterface $userRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function __construct(
        ClientRepositoryInterface $clientRepository,
        ScopeRepositoryInterface $scopeRepository,
        AccessTokenRepositoryInterface $accessTokenRepository,
        UserRepositoryInterface $userRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository
    ) {
        $this->clientRepository = $clientRepository;
    }
    
}