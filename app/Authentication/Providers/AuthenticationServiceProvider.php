<?php

declare(strict_types=1);

namespace App\Authentication\Providers;

use Illuminate\Support\ServiceProvider;
use App\Core\Providers\Concerns;

/**
 * Class AuthenticationServiceProvider
 * @package App\Authentication\Providers
 */
class AuthenticationServiceProvider extends ServiceProvider
{
    use Concerns\HasAliases,
        Concerns\HasMiddleware;
    
    /**
     * @var bool
     */
    protected $defer = true;
    
    /**
     * @var array
     */
    protected $aliases = [];
    
    /**
     * @var array
     */
    protected $middleware = [];
    
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->bootConfiguration();
    }
    
    /**
     * @return void
     */
    protected function bootConfiguration(): void
    {
        $this->app->configure('api');
        $this->app->configure('app');
        $this->app->configure('filesystems');
        $this->app->configure('logging');
        $this->app->configure('schema');
    }
    
    /**
     * @return void
     */
    public function register(): void
    {
        $this->registerAliases();
        $this->registerMiddleware();
    }
    
}
