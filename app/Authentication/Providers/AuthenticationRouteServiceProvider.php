<?php

declare(strict_types=1);

namespace App\Authentication\Providers;

use App\Core\Providers\CoreRouteServiceProvider;

/**
 * Class AuthenticationRouteServiceProvider
 * @package App\Authentication\Providers
 */
class AuthenticationRouteServiceProvider extends CoreRouteServiceProvider
{
    
    protected $defer = true;
    
    protected $attributes = [
        'prefix'     => 'oauth',
        'namespace'  => 'App\\Authentication\\Http\\Controllers',
        'middleware' => [],
    ];
    
    /**
     * @return \Closure
     */
    public function getRouteCallback(): \Closure
    {
        return function(){
            $this->getRouter()->post('token', 'AuthenticationController@issue');
        };
    }
    
    
}
