<?php

declare(strict_types=1);

namespace App\User\Http\Requests;

use Laravel\Lumen\Http\Request;

/**
 * Class FetchUserRequest
 * @package App\User\Http\Requests
 */
class FetchUserRequest extends Request
{

}
