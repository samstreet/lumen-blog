<?php

declare(strict_types=1);

namespace App\User\Http\Controllers;

use App\User\Http\Requests\FetchUserRequest;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    
    /**
     * @param FetchUserRequest $userRequest
     *
     * @return JsonResponse
     */
    public function index(FetchUserRequest $userRequest): JsonResponse
    {
        return response()->json(["hello" => "world"]);
    }
    
}
