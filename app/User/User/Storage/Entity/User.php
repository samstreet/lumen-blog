<?php

declare(strict_types=1);

namespace App\User\Storage\Entity;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];
    
    /**
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    
}