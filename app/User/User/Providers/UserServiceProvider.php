<?php

declare(strict_types=1);

namespace App\User\Providers;

use App\Application;
use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Cache\Factory as CacheFactory;
use Illuminate\Support\ServiceProvider;
use App\Core\Providers\Concerns;

class UserServiceProvider extends ServiceProvider
{
    
    use Concerns\HasAliases;
    
    /**
     * {@inheritdoc}
     */
    protected $defer = true;
    
    /**
     * {@inheritdoc}
     */
    protected $aliases = [
        'cache' => [CacheFactory::class, CacheManager::class]
    ];
    
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->bootConfiguration();
    }
    
    /**
     * @return void
     */
    protected function bootConfiguration(): void
    {
        $this->app->configure('api');
        $this->app->configure('app');
        $this->app->configure('filesystems');
        $this->app->configure('logging');
        $this->app->configure('schema');
    }
    
    /**
     * @return void
     */
    public function register(): void
    {
        $this->registerAliases();
        $this->registerCacheManager();
    }
    
    /**
     * @return void
     */
    protected function registerCacheManager(): void
    {
        $this->app->singleton('cache', function (Application $app) {
            return new CacheManager($app);
        });
    }
    
}
