<?php

declare(strict_types=1);

namespace App\User\Providers;

use App\Core\Providers\CoreRouteServiceProvider;

/**
 * Class UserRouteServiceProvider
 * @package App\User\Providers
 */
class UserRouteServiceProvider extends CoreRouteServiceProvider
{
    
    protected $defer = true;
    
    /**
     * {@inheritdoc}
     */
    protected $attributes = [
        'prefix'     => 'users',
        'namespace'  => 'App\\User\\Http\\Controllers',
        'middleware' => [],
    ];
    
    /**
     * @return \Closure
     */
    public function getRouteCallback(): \Closure
    {
        return function(){
          $this->getRouter()->get('me', 'Controller@index');
        };
    }
    
    
}