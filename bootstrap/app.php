<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

$app = new App\Application(realpath(__DIR__ . '/../'));
$app->registerLocalServiceProviders();
$app->registerServiceProviders();
$app->withEloquent();

return $app;